from subprocess import PIPE, Popen
import pathlib
import shlex

def shrun(command, _input: bytes) -> tuple[bytes, int]:
    """get a subrocess result and return code"""
    command = command if isinstance(command,list) else shlex.split(command)
    proc = Popen(command, stdin=PIPE, stdout=PIPE)
    result, error = proc.communicate(_input)
    return result, proc.returncode

def code_messages(messages) -> dict:
    """convert a list of messages to a dict of int codes"""
    messages = messages if isinstance(messages,list) else messages.split(",")
    return {codenum : " ".join(message.split()) for codenum, message in zip(
        (int(c) for c in range(len(messages))),
        (m.replace("\n"," ").strip() for m in messages))}

def query(_input: bytes, expr: str) -> bytes:
    """query an input with XPath expression"""
    messages = code_messages("""
        no error,,,,,,, error in pattern, error in reader registration, out of
        memory error, XPath evaluation error""")
    command = f"xmllint --xpath {expr} -"
    result, rcode = shrun(command, _input)
    error = messages[rcode] if rcode not in (0,10) else None
    if error is None: return result.decode() # strings
    print(error)

def transform(_input: bytes, xslt: pathlib.Path, **params) -> bytes:
    """let xsltproc transform an input using provided stylesheet and params"""
    messages = code_messages("""
        normal, no argument, too many parameters, unknown option, failed to
        parse the stylesheet, error in the stylesheet, error in one of the
        documents, unsupported xsl:output method, string parameter contains
        both quote and double-quotes, internal processing error, processing
        was stopped by a terminating message, could not write the result to
        the output file""")
    command = "xsltproc --encoding UTF-8".split()
    for key,value in params.items():
        command.extend(("--stringparam",key,value))
    command.extend((xslt,"-"))
    result, rcode = shrun(command, _input)
    error = messages[rcode] if rcode != 0 else None
    return result, error

def from_html(_input: bytes) -> bytes:
    """parse an input string as XHTML"""
    messages = code_messages("""
        No error, Unclassified, Error in DTD, Validation error, Validation
        error,,, Error in pattern,, Out of memory error""")
    command = """xmllint  --html --nsclean --noent --noblanks
         --encode utf-8 --xmlout --dropdtd --nodefdtd --c14n -"""
    result, rcode = shrun(command, _input)
    error = messages[rcode] if rcode != 0 else None
    return result, error

def validate(_input: str, path: pathlib.Path):
    """call xmllint to validate an input"""
    _ext = path.suffix.lower().lstrip(".")
    _xvalid = {"dtd":"dtdvalid","rng":"relaxng","xsd":"schema"}
    _model = _ext if _ext in (_xvalid.keys()) and path.exists() else None
    message,rcode = f"Unsupported model {path}", 4
    messages = code_messages("""
        No error, Unclassified, Error in DTD, Validation error, Validation
        error,,, Error in pattern,, Out of memory error""")
    if _model:
        command = ["xmllint"]
        #if _ext in ("rng","xsd"): command.append(" --stream ")
        command.append(f"--{_xvalid[_model]}")
        command.append(str(path.absolute()))
        command.append("-")
        message,rcode = shrun(command, _input)
        if rcode != 0:
            message=message.split("validity error: ")[1].split("\n")[0]
    return (rcode==0, message)

