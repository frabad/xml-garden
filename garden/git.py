import datetime
import subprocess
import pathlib

class Repository(object):
    """mostly useless class for building data from current repository"""

    def __init__(self):
        """gather metadata from Git repo"""

        def shrun(cmd):
            """get info from a subprocess command"""
            return subprocess.run(cmd.split() if isinstance(cmd,str) else cmd,
                stdout=subprocess.PIPE,check=True).stdout.decode().strip()
    
        self.name, self.description, self.version = None, None, None
        desc_file = (pathlib.Path("..") / ".git" / "description")
        if desc_file.exists():
            self.name, self.description = [i.strip() for i in
                                           desc_file.read_text().split(":")]
        self.url = shrun("git config --get remote.origin.url")
        self.author = shrun("git config --get user.name")
        self.author_email = shrun("git config --get user.email")
        date_stamp = shrun("git log -1 --format=%ct")
        date = datetime.datetime.fromtimestamp(int(date_stamp)).timetuple()
        self.version = f"{date.tm_year}.{date.tm_yday}"# %Y.%j for PEP396
    