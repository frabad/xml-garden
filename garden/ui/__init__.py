try:
    from .tk import *
except:
    from .console import *
    print("Consider installing the 'Tkinter' module. ",
          "Falling back to console user interface.")

