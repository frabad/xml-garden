#!/bin/env python
"""
GTK utils for Garden XML

note:
    This script was designed for the lightweight Geany IDE.
    It is supposed to be called from a custom command
    to process the standard input.

"""
import gi
gi.require_version("Gtk", "3.0")
import gi.repository.Gtk as gtk
import sys

class SingleEntryPopup(gtk.Window):
    """simple popup with one focused entry and no button"""
    
    def __init__(self,title):
        """"""
        
        def save(event):
            """save the value and quit"""
            self.value = self.entry.get_text()
            close(event)
            
        def close(event):
            """quit"""
            gtk.main_quit()
        
        self.value = None
        super().__init__(title=title)
        self.connect("destroy", close)
        self.entry = gtk.Entry()
        self.entry.connect("activate", save)
        self.box = gtk.Box()
        self.box.pack_start(self.entry, True, True, 0)
        self.add(self.box)
        self.show_all()
        gtk.main()


def text2tag(text, tag_name):
    """surround text with an XML element named after a popup query"""
    popup = SingleEntryPopup(tag_name)
    tag = (popup.value or tag_name).strip().replace(" ","_")
    return f"""<{tag}>{text}</{tag}>"""

if __name__ == "__main__":
    assert not(sys.stdin.isatty()), __doc__
    text = "".join(i for i in sys.stdin.readlines())
    tag_name = sys.argv[1] if len(sys.argv)>1 else "XML tag name"
    sys.stdout.write(text2tag(text,tag_name))
