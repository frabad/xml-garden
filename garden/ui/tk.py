import tkinter
import xml.etree.ElementTree as ET

class SingleEntryPopup(object):
    """simple popup with one focused entry and no button"""
    
    def __init__(self, title):
        """"""
        
        def save(event):
            """save the value and quit"""
            self.value = self.entry.get()
            close(event)
            
        def close(event):
            """quit"""
            self.master.destroy()
        
        self.value = None
        self.master = tkinter.Tk()
        self.master.resizable(True,False)
        self.master.title(title)
        self.master.bind("<Escape>",close)
        self.master.protocol("WM_DELETE_WINDOW",close)
        self.entry = tkinter.Entry(self.master)
        self.entry.bind("<Return>",save)
        self.entry.pack(fill=tkinter.X,padx=(10,10),pady=(10,10))        
        self.entry.focus()
        self.master.mainloop()


class FieldSet(object):
    """fieldset drawer and reader"""
    
    def __init__(self, xml):
        self.xml = xml
        self._fields = []
        self.master = tkinter.Tk()
        self.draw()
    
    def draw(self):
        """draw main controls and attach events"""
            
        def add_field(field, position):
            """draw a field at given position"""
            label = tkinter.Label(self.master,text=field.tag)
            label.grid(row=position,sticky=tkinter.E)
            entry = tkinter.Entry(self.master,width=60,relief=tkinter.FLAT)
            entry.insert(0,field.text or "")
            entry.grid(row=position,column=1,sticky=tkinter.W,padx=(4,0))
            self._fields.append(entry)
        
        for position, field in enumerate(self.xml):
            add_field(field, position)
        button = tkinter.Button(self.master,width=6,text="OK",command=self.save)
        button.grid(columnspan=2,pady=4,sticky=tkinter.S)
        self.master.title(self.xml.tag)
        self.master.resizable(False, False)
        self.master.protocol("WM_DELETE_WINDOW", self.master.destroy)
        self.master.mainloop()
    
    def save(self):
        """update tree with fields values"""
        for i,field in enumerate(self._fields):
            self.xml[i].text=field.get()
        self.master.destroy()
    
    def __str__(self):
        """make human readable"""
        return ET.tostring(self.xml, encoding="unicode")


