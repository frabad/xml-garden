"""SGML-related wrappers and functions

The consistent way of calling these functions is to capture the output as a
two-item tuple, where the first item is the expected content and the second is
a context error, basically the result of a handled exception.  One of the two
items must be None. The end-user process should reliably display the error when
the expected content is None.

"""
import io
import pathlib
import re
from subprocess import PIPE, Popen
from typing import Optional, Iterable

def _ents_declare(names: str, sdata: bool=True) -> Iterable:
    """Generate a series of entity declarations either as general or SDATA.

    example:
        print("\n".join(_ents_declare(
            "eacute nbsp egrave Eacute ndash",1)))
    """
    notation = "SDATA" if sdata else ""
    out = lambda name: f"""<!ENTITY {name} {notation} "[|{name}|]">"""
    for i in sorted(set(names.split())): yield out(i)

def _ents_to_pis(input: bytes) -> bytes:
    """convert sdata entities in a stream to an SGML processing-instruction

    Every &eacute; becomes <?ent eacute> in the SGML stream, to be later
    converted to the XML <?ent eacute?> by SP. XML-reserved names are not
    processed.

    """
    entity = re.compile(r"\&((?!lt|amp|quot|apos|gt)[^;]*);")
    pis = lambda x: entity.sub(r"<?entity \1>", x.decode()).encode("utf-8")
    stream = io.BytesIO()
    for line in io.BytesIO(input).readlines():
        stream.write(pis(line))
    stream.seek(0)
    return stream.read()

def from_path(fp: pathlib.Path, protect_entites=True
              ) -> tuple[Optional[bytes],Optional[bytes]]:
    """Parse SGML from file."""
    data, errors = None, None
    with fp.open("rb") as stream:
        bytes = stream.read()
        if protect_entites:
            #garden.log("Convert entities to processing instructions.")
            bytes = _ents_to_pis(bytes)
        #garden.log("Convert SGML stream to XML.")
        data, errors = _sp_run(bytes, fp.parent)
    if errors:
        # dump SGML errors
        err_fp = fp.with_suffix(fp.suffix+".err")
        errors = io.BytesIO(errors).readlines()
        with err_fp.open("wb") as f:
            for e in errors: f.write(e)
    return data, errors

def _sp_run(input: bytes, cwd: pathlib.Path=None, exe="osx"
    ) -> tuple[Optional[bytes],Optional[bytes]]:
    """Capture the standard output and standard error of an SP call.

    To deal with OSP/OSX strange behaviour -where no STD output nor STD
    error is returned-, the subprocess return code is checked. OSX
    randomly returns an absurd value (3221225477 or so).

    The logic here is to run the subprocess again until we get one of
    the expected return codes 0 ('OK') or 1 ('error').

    Also note that providing anything else than bytes or string as the
    input seems to ruin the aforementioned logic.

    """
    sp_commands = {
        "sx": {
            "args": (
                "-bUTF-8", # force utf-8 output encoding
                "-E0"      # disable error limit
                ),
            "url": "http://www.jclark.com/sp/",
            },
        "osx": {
            "args": (
                "-bUTF-8", # force utf-8 output encoding
                #"-C","catalog", # supposedly implicit
                ),
            "url": "https://openjade.sourceforge.net/"
            },
    }
    args, url = sp_commands[exe]["args"], sp_commands[exe]["url"]
    #garden.require(shutil.which(exe), (f"'{exe}' must be in your PATH.\nGet it from <{url}>."))
    cmd = [exe]+[i for i in args]
    p = Popen(cmd, cwd=cwd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    data, error = p.communicate(input)
    data, error = data or None, error or None # Exclude empty results.
    if all((data is None, error is None, p.returncode>1)): # Shouldn't happen.
        data, error = _sp_run(input, cwd) # Too bad. Try again.
    return data, error
