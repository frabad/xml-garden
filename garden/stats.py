import xml.etree.ElementTree as ET
import importlib.resources

XMLNS={
    "rng":"http://relaxng.org/ns/structure/1.0",
    "xsl":"http://www.w3.org/1999/XSL/Transform"
}

def ratio(nm, dnm, percent=False):
    """return the ratio of two values"""
    r = nm / dnm
    return "{:.2%}".format(r) if percent else r

def avg(d):
    """average the values in a dict"""
    return sum(d.values())/len(d.values())

def countchars(tree=None):
    """count characters in an XML document
    
    Returns:
        characters count as int
    
    """
    xsl = importlib.resources.files("garden") / "xsl/countchars.xsl"
    result, error = tree.xslt(xsl)
    assert (error is None
        ),(f"[{xsl}]: {error}")
    return int(result) if tree else 0

class Elements(object):
    """"""

    def __init__(self, rng, xml, xslt=None):
        """"""
        self.rng  = ET.parse(str(rng))
        self.xml  = ET.parse(str(xml))
        self.xslt = ET.parse(str(xslt)) if xslt else None
        
        self.defined = self._defined()
        self.used = self._used()
        self.ignored = self.defined.difference(self.used)
        self.matched = self._matched() if self.xslt else None
        self.selected = self._selected() if self.xslt else None
        self.queried = self.matched.union(self.selected) if self.xslt else None

    def _defined(self):
        """gather the elements defined in an RNG model
        
        Returns:
            a set of elements names
        
        """
        return set([i.attrib['name'] for i in self.rng.findall(
            ".//rng:element[@name]", XMLNS)])

    def _valid_queries(self,expressions):
        """find XSLT expressions that match a list of definitions"""
        
        def msplit(query, seps="/[]|()"):
            """split XPath expression"""
            for sep in seps:
                query = query.replace(sep," ")
            return query.split()
            
        queries = [i for i in set(
            msplit(" ".join(expressions))
            ) if i in self.defined]
        return set(queries)

    def _used(self):
        """gather the elements used in a document
        
        Returns:
            a set of elements names
        
        """
        return set(
            [i.tag for i in self.xml.findall(".//*")]+[self.xml.getroot().tag]
            ).intersection(self.defined)
    
    def _selected(self):
        """gather XPath selections from an XSL-T
        
        Returns:
            a set of elements names
        
        """
        selections = [i.attrib['select'] for i in self.xslt.findall(
            ".//xsl:*[@select]", XMLNS)]
        return self._valid_queries(selections)

    def _matched(self):
        """gather the template matches from an XSL-T
        
        Returns:
            a set of elements names
        
        """
        matches = [i.attrib['match'] for i in self.xslt.findall(
            ".//xsl:template[@match]", XMLNS)]
        return self._valid_queries(matches)

