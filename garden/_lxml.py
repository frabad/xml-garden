"""XML processing functions handled by LXML"""
import lxml.etree as LX
from typing import Optional
import pathlib 

def _log_headline(log) -> Optional[str]:
    """Format the first line of an error_log, i.e. the main error.

    see:
        https://lxml.de/parsing.html#error-log
    
    """
    log = log.filter_from_errors()[0]
    return "{} near line {}, col {}.".format(
        log.message, log.line, log.column)

def parse(input) -> tuple[Optional[LX._Element],Optional[str]]:
    """controlled XML document parse method using LXML"""
    parser, xml, error = LX.XMLParser(), None, None
    try:
        match input:
            case pathlib.Path():
                xml = LX.parse(input,parser).getroot()
            case str() | bytes():
                xml = LX.XML(input,parser)
            case LX._ElementTree():
                xml = input.getroot()
            case LX._Element():
                xml = input
            case _:
                error = "unparsable or unsupported input type"
    except LX.LxmlError:
        error = _log_headline(parser.error_log)
    return xml, error

def validate(xml, model_path):
    """validate an XML document against a model

    TODO: add SVRL / Schematron support
    """

    def validator(path):
        """validation processor"""
        ext = path.suffix.lower().lstrip(".")
        if ext == "dtd":
            return LX.DTD(str(path))
        if ext == "rng":
            return LX.RelaxNG(LX.parse(str(path)))
        if ext == "xsd":
            return LX.XMLSchema(LX.parse(str(path)))
    model = validator(model_path)
    valid = model.validate(xml)
    error = ( "line %s: %s" % (
        model.error_log[0].line, model.error_log[0].message
        ) if len(model.error_log) > 0 else None)
    return valid, error

def schematron(xml, svrl_path):
    """generate an ISO Schematron report with the provided SVRL rules

    This relies on an XSL-T call. The internal lxml schematron
    mechanism doesn't seem to work for some reason.

    """
    report, _ = transform(xml, svrl_path)
    return str(report).strip() or None

def transform(xml, xsl_path, parameters={}) -> tuple[Optional[str], Optional[str]]:
    """run an XSL-T transformation"""
    result, error = None, None
    transform = LX.XSLT(LX.parse(str(xsl_path)))
    for k,v in parameters.items():
        parameters.update({k: LX.XSLT.strparam(v)})
    try:
        result = transform(xml, **parameters)
    except(LX.XSLTApplyError):
        error = []
        for i in transform.error_log:
            if i.filename != "<string>":
                error.append(f"{i.filename}")
            if i.line != 0:
                error.append(f"line {i.line}")
            if i.message != "":
                error.append(f"{i.message}")
        error = ": ".join(error)
    return result, error

def to_file(fp: pathlib.Path, xml) -> tuple[bool,Optional[str]]:
    """write XML to file"""
    error = None
    # make sure xml is an etree
    if isinstance(xml,LX._Element):
        xml = LX.ElementTree(xml)
    try:
        with fp.open('w'):
            xml.write(fp, pretty_print=True, encoding="utf-8",
                      xml_declaration=True)
    except LX.LxmlError as e:
        error = e
    return fp.exists() and fp.stat().st_size > 0, error

