"""hub interface for various processing functions, found either in other
modules or locally installed programs"""

import xml.etree.ElementTree as ET
import importlib.resources
import garden.libxml2
import garden.text
#import markdown as MD
import pathlib
from typing import Optional
import urllib.request

def _grow(element):
    """append siblings elements named after their parent Element

    Split the text list in the parent element if its name has an 's' suffix.

    """
    def _singularize(token):
        """make a singular name from its plural form"""
        return token[:-1] if token[-1].lower() == "s" else token
    
    singular = _singularize(element.tag)
    if (element.tag is not singular
        and element.text is not None and len(element) == 0):
        for value in element.text.split(","):
            sibling = ET.SubElement(element, singular)
            sibling.text = value.strip()
        element.text=None
    else:
        for child in element:
            _grow(child)
    return element

def grow(self):
    """grow the contents of an ElementTree"""
    _grow(self.getroot())

#TODO: move to Tree
ET.ElementTree.grow = grow

def from_tuples(seed):
    """create a tree with an element for every tuple

    Returns:
        an Element instance

    TODO:
        add support for tail

    """
    if isinstance(seed[0],str):
        elem = ET.Element(seed[0])
        if len(seed) > 1:
            for content in seed[1:]:
                if isinstance(content,tuple):
                    elem.append(from_tuples(content))
                if isinstance(content, str):
                    if elem.text:
                        elem.text = " ".join([elem.text,content])
                    else:
                        elem.text = content
        return elem

def _indent(elem, tab, level=0):
    """indent the contents of an Element

    http://effbot.org/zone/element-lib.htm#prettyprint

    """
    i = "\n" + level*tab
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + tab
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            _indent(elem, tab, level=level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def indent(self, tab="  "):
    """indent the contents of an ElementTree"""
    _indent(self.getroot(), tab=tab)

#TODO: move to Tree
ET.ElementTree.indent = indent

def from_markdown(text:str):
    """convert markdown contents to a customized xHTML tree"""
    _md = MD.Markdown(
        extensions=[
            'MD.extensions.def_list',
            'MD.extensions.extra',
            'MD.extensions.footnotes',
            'MD.extensions.meta',
            'MD.extensions.toc',
            'MD.extensions.tables',
        ]
    )
    html = ET.Element('html',attrib={"xmlns":"http://www.w3.org/1999/xhtml"})
    head = ET.SubElement(html, 'head')
    content = ET.XMLParser()
    contents = ("<div>",_md.convert(text),"</div>")
    for i in contents: content.feed(i)
    body = ET.SubElement(html,'body')
    body.extend(content.close())
    title = body.find('h1').text if body.find('h1') is not None else ""
    htitle = ET.SubElement(head,"title")
    htitle.text = title
    mtitle = ET.Element("meta",attrib={"name":"title","content":title})
    head.append(mtitle)
    script = ET.SubElement(head,"script",attrib={
        "type":"text/javascript","src":"script.js"
    })
    script.text = "/*  */"
    style = ET.SubElement(head,"link",attrib={
        "rel":"stylesheet","type":"text/css","href":"style.css"})
    for i in _md.Meta:
        value = _md.Meta[i][0]
        if i.lower() == "lang":
            html.attrib=({"lang":value,"xml:lang":value})
        else:
            meta = ET.SubElement(head,"meta")
            if i.lower() == "charset":
                meta.attrib={
                    "http-equiv":"Content-Type",
                    "content": f"text/html; charset={value}"}
            else:
                meta.set("name",i)
                if i.lower() == "keywords" or i.lower() == "tags":
                    meta.set("content",",".join(
                        [v.strip() for v in value.split(",")]))
                else:
                    meta.set("content",value)
    return ET.ElementTree(html)

def from_path(fp: pathlib.Path) -> tuple[Optional[ET.Element],Optional[str]]:
    """attempt to parse an XML document from a path"""

    def h_readable(self) -> bool:
        """parse text file for unicode correct decoding"""
        try:
            with self.open("r") as f:
                for i in f:  pass
        except UnicodeDecodeError: # binary character encountered
            return False
        return True
    
    data, error = None, None
    if not all((fp.exists(),fp.is_file(),fp.stat().st_size>0,h_readable(fp))):
        error = f"'{fp}' must be an existing, non-empty, non-binary file."
        garden.log(error)
    if error is None:
        try:
            data = ET.ElementTree(ET.XML(fp.read_bytes()))
        except ET.ParseError as e:
            error = str(e).replace(":"," near")
    return data, error

def from_http(url:str):
    """read an elementTree from an HTTP request response"""
    
    class Http(object):

        def __init__(self, url:str):
            """"""
            headers = {"User-Agent": (
                "Mozilla/5.0 (Windows NT 6.1)"
                "AppleWebKit/537.36 (KHTML, like Gecko)"
                "Chrome/41.0.2228.0"
                "Safari/537.3")}
            url = "http://"+url if not url.startswith("http") else url
            req = urllib.request.Request(url, None, headers)
            with urllib.request.urlopen(req) as rsp:
                self.content_type = rsp.headers.get_content_type()
                self.content_charset = rsp.headers.get_content_charset()
                self.content = rsp.read()
        
        def get(self) -> tuple[Optional[bytes],Optional[str],Optional[str]]:
            """HTTP response"""
            data, encoding, error = self.content, self.content_charset, None
            if self.content_type != "text/html":
                error = f"{self.content_type} is not supported."
            return data, encoding, error
    
    data, encoding, error = Http(url).get()
    if error is None:
        data, error = garden.libxml2.from_html(data)
        data = ET.ElementTree(ET.fromstring(data))
    return data, error

def to_path(self, path, **kwargs):
    """write an ElementTree to a path"""
    with open(path,"w"):
        self.write(str(path), **kwargs)

#TODO: move to Tree
ET.ElementTree.to_path = to_path

def xlang(self, lang):
    """make an XML tree that contains one lang only

    Filters out the XML nodes that do not match the lang provided as parameter.
    This is done by processing the tree with a specific XSL-T.

    """
    xsl = importlib.resources.files("garden") / "xsl/xlang.xsl"
    result, error = self.transform(xsl, lang=lang)
    assert(error is None
        ),(f"[{xsl}]: {error}")
    assert(result is not None
        ),("result tree is empty")
    return result

#TODO: move to Tree
ET.ElementTree.xlang = xlang

def transform(self, xslt_path:pathlib.Path, **params):
    """transform an elementtree using a stylesheet
    
    The transformation occurs on the ElementTree root element, which is first
    converted to bytes using the 'tostring' method with no encoding parameter.

    """
    _input = ET.tostring(self.getroot())
    result, error = garden.libxml2.transform(_input, xslt_path, **params)
    try:
        result = ET.XML(result)
    except Exception:
        result = result or None # do not output empty string
    return result, error

#TODO: move to Tree
ET.ElementTree.transform = transform

def query(self, expr:str):
    """"""
    _input = ET.tostring(self.getroot())
    return garden.libxml2.query(_input, expr)

#TODO: move to Tree
ET.ElementTree.query = query

def validate(self, path):
    """"""
    _input = ET.tostring(self.getroot())
    return garden.libxml2.validate(_input, path)

#TODO: move to Tree
ET.ElementTree.validate = validate
