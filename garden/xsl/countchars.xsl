<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" />
  <xsl:template match="/*[1]">
    <xsl:param name="contents" select="normalize-space(.)"/>
    <xsl:value-of select="number(string-length($contents))"/>
  </xsl:template>
</xsl:stylesheet>

