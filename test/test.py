#!/usr/bin/env python
import unittest
import garden
import pathlib
import urllib.request
import importlib.resources

BASE_DIR = importlib.resources.files("test")
SAMPLES = BASE_DIR / "samples"

class TestParse(unittest.TestCase):

    def test_xml_from_string(self):
        """parsing sans erreur"""
        x = """
            <contents xml:lang="any">
                <empty/>
            </contents>

            """
        tree = garden.etree.ET.XML(x)
        self.assertIsNotNone(tree)
    
    def test_xml_query(self):
        p = pathlib.Path(SAMPLES / "1.xml")
        tree,error = garden.etree.from_path(p)
        self.assertIsNotNone(tree,error)
        self.assertEqual(tree.query("//contents/simple/text()"),"with text")

    def _test_xml_utf8_name(self):
        """test is disabled until characters in sample are properly converted to
        UTF-8 and not escaped, i.e. é is not &#233;"""
        p = pathlib.Path(SAMPLES / "profil.skyeye.xml")
        tree,error = garden.etree.from_path(p)
        self.assertIsNone(error,error)
        self.assertIsNotNone(tree)
        self.assertEqual(tree.query("//*[1]"),"Henri")
    
    def test_error_in_xml(self):
        """analyse de fichiers XML et reconnaissance des erreurs"""
        p = pathlib.Path(SAMPLES / "1.xml")
        tree,error = garden.etree.from_path(p)
        self.assertIsNotNone(tree,f"{error} in {p}")
        for xmlnot in ("1.err.xml","2.err.xml","3.err.xml"):
            p = SAMPLES / xmlnot
            tree,error = garden.etree.from_path(p)
            self.assertIsNone(tree,f"could not find error in {p}")

    def test_error_in_sgml(self):
        """analyse de fichiers SGML et reconnaissance des erreurs"""
        p = pathlib.Path(SAMPLES / "4.invalid.sgml")
        _,error = garden.sgml.from_path(p)
        self.assertIsNotNone(error,f"{error} in {p}")

    def test_xml_from_http(self):
        """get a tree from an HTTP response"""
        url = "http://example.com"

        def internet_on():
            try:
                urllib.request.urlopen(url, timeout=1)
                return True
            except urllib.request.URLError:
                return False

        if internet_on:
            #this might take long
            tree, error = garden.etree.from_http(url)
            self.assertIsNone(error)
            #TODO: separate xpath test
            self.assertEqual(tree.query("//title/text()"),"Example Domain")

class TestXSLT(unittest.TestCase):
    
    def test_xlang(self):
        """XSL-T xlang"""
        xmlns = "http://www.w3.org/XML/1998/namespace"
        #nsmap = {"xml":xmlns}
        x = """
            <elements xml:lang="any">
                <element xml:lang="fr">texte</element>
                <element xml:lang="en">text</element>
            </elements>
            """
        tree = garden.etree.ET.ElementTree(garden.etree.ET.XML(x))
        self.assertIsNotNone(tree)
        for lang in ("en","fr"):
            self.assertEqual(tree.xlang(lang).attrib.get(
                "{%s}lang" % (xmlns)),lang)


class TestTODO(unittest.TestCase):

    def todo_test_validate_valid(self):
        """validate"""
        tree = garden.ElementTree(tree.xlang("fr"))
        valid, error = tree.validate(pathlib.Path("profils.xsd"))
        #self.assertFalse(valid,"Le document contient une erreur:\n\t%s" % (error))

    def todo_test_xpath(self):
        """xpath"""
        tree = garden.ElementTree(tree.xlang("fr"))
        print(tree.xpath("//*[1]//qualités/*[2]"))

    def todo_test_html(self):
        """import html"""
        p = pathlib.Path("test.html")
        tree, error = garden.libxml2.xhtml(p)


if __name__ == '__main__':
    unittest.main()

